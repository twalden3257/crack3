
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings
int line_count = 0; // Needs to be golbal

//Given a filename, return its length or -1 if error
int file_length(char *filename)
{
    struct stat fileinfo;
    if (stat(filename, &fileinfo) == -1)
        return -1;
    else
        return fileinfo.st_size;
}

char ** readfile(char *filename)
{
    //obtain length of file
    int len = file_length(filename);
    if (len == -1)
    {
        printf("Couldn't get length of file %s\n", filename);
        exit(1);
    }
    
    //allocate memory for the entire file
    char *file_contents = malloc(len);
    
    //Read entire file into file_contents
    FILE *fp = fopen(filename, "r");
    if (!fp)
    {
        printf("Couldn't open %s for reading\n", filename);
        exit(1);
    }
    fread(file_contents, 1, len, fp);
    fclose(fp);
    
    //replace \n with \0
    //also keep a count as we progress
    line_count = 0;
    
    for (int i = 0; i < len; i++)
    {
        if (file_contents[i] == '\n')
        {
            file_contents[i] = '\0';
            line_count++;
        }
    }
    
    //Allocate array of pointers to each line, with additional space for NULL at end
    char **lines = malloc((line_count+1) * sizeof(char *));
    
    //Fill in each entry with address of corresponding line
    int c = 0;
    for (int i = 0; i < line_count; i++)
    {
        lines[i] = &file_contents[c];
        
        //Scan forward to find next line
        while (file_contents[c] != '\0') c++;
        c++;
    }
    
    //Store NULL at end of the array
    lines[line_count] = NULL;
    
    //Return the address of the data structure
    //printf("%s\n", &file_contents[0]);
    return lines;
}



// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
/*
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    
    
    // Compare the two hashes

    // Free any malloc'd memory

    return 0;
}
*/

struct entry 
{
    char password[PASS_LEN];
    char hash[HASH_LEN];
};

int hash_cmp(const void *a, const void *b)
{
    struct entry *temp1 = (struct entry*)a;
    struct entry *temp2 = (struct entry*)b;
    // qsort expects two arguments that point to the objects being compared.
    return strcmp(temp1->hash, temp2->hash);
}

int binary_cmp(const void *a, const void *b)
{
    struct entry *temp2 = (struct entry *)b;
    // The compar routine is expected to have two arguments 
    // which point to the key object and to an array member
    return strcmp( a , temp2->hash  );
}

// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    *size = file_length(filename);
    char **dictionary = readfile(filename);
    struct entry *temp = malloc(line_count*sizeof(struct entry));
    
    for(int i = 0; i<line_count; i++)
    {
        strcpy(temp[i].password, dictionary[i]);
        //char *temp = md5(temp[i].password, strlen(temp[i].password));
        strcpy(temp[i].hash, md5(temp[i].password, strlen(temp[i].password)));
    }
    
    return temp;
    
    free(dictionary[0]);
    free(dictionary);
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    //char **rockyou = readfile(argv[2]);

    
    // TODO: Read the dictionary file into an array of entry structures
    int dict_len;
    struct entry *dict = read_dictionary(argv[2], &dict_len);

    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, line_count, sizeof(struct entry), hash_cmp);

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    FILE *fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Can't open %s for reading \n", argv[1]);
        exit(1);
    }
    
    //Using bsearch
    char line[100];
    while(fgets(line, HASH_LEN+1, fp) != NULL)
    {   
        line[32] = '\0'; //Replaces \n with null
        char *match = bsearch(line, dict, line_count, sizeof(struct entry), binary_cmp);
        if (match != NULL)
        {
            printf("Hash found!  %s is %s\n", line, match);
        }
        
    }
    fclose(fp);
    

}
